import React from "react";
import { Text, StyleSheet, View, Button, TouchableOpacity, TextInput, Image } from "react-native";

const App = ({})=>{
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <View style={{backgroundColor: '#002558', 
        paddingHorizontal: 30,
        padding: 12, borderRadius: 30, marginTop: -25, alignSelf: "center"}}>
          <Text style={{color: 'white', fontSize: 18, fontWeight: 'bold'}}>Digital Approval</Text>
        </View>

        <View>
        <Image source={require('./src/images/logo.png')}
        style={{width: 200, height: 100, resizeMode: 'contain', alignSelf: 'center', marginVertical: 20,}}/>
        </View>

        <View>
        <TextInput value="" placeholder="Alamat Email" style={styles.inputText}/>
        
        </View>
        <Image source={require('./src/images/mailicon.png')}
        style={{width: 20, height: 20, resizeMode: 'contain', marginTop: -60,
        marginHorizontal: 35,}}/>
        
        <View>
        <TextInput value="" placeholder="Alamat Email" style={styles.inputText}/>
        
        </View>


        <Image source={require('./src/images/lockicon.png')}
        style={{width: 20, height: 20, resizeMode: 'contain', marginTop: -60,
        marginHorizontal: 35,}}/>
    
        <TouchableOpacity style={{margin: 30,
        alignSelf: 'flex-end'}}>
          <Text style={{fontSize: 18, color: '#287AE5', fontStyle: 'italic',
        fontWeight:'bold'}}>Reset Password</Text>
        </TouchableOpacity>

        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.appButtonContainer}>
            <Text style={{fontWeight: 'bold', 
            fontSize: 16, 
            textTransform: "uppercase", 
            color: 'white', textAlign: 'center'}}>Login</Text>
            </TouchableOpacity>
      </View>
    </View>
  )
}

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    padding: 16,
    marginBottom: 20,
    backgroundColor: '#0000000F',
  },
  appButtonContainer: {
    backgroundColor: "#287AE5",
    borderRadius: 10,
    paddingVertical: 20,
    marginHorizontal: 20,
    marginBottom: 20,
  },
  content: {
    backgroundColor: '#FFFFFF',
    marginHorizontal: 20,
    borderRadius: 10,
  },
  inputText: {
    paddingLeft: 45,
    fontWeight: 'bold',
    borderRadius: 10,
    borderColor: '#002558',
    borderWidth: 0.7,
    marginHorizontal: 20,
    marginVertical: 25,
    
  },
})